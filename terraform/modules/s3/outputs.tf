output "bucket_domain_name" {
  value       = "${var.enabled == "true" ? join("", aws_s3_bucket.s3_bucket.*.bucket_domain_name) : ""}"
  description = "FQDN of bucket"
}

output "bucket_id" {
  value       = "${var.enabled == "true" ? join("", aws_s3_bucket.s3_bucket.*.id) : ""}"
  description = "Bucket Name (aka ID)"
}

output "bucket_arn" {
  value       = "${var.enabled == "true" ? join("", aws_s3_bucket.s3_bucket.*.arn) : ""}"
  description = "Bucket ARN"
}

output "prefix" {
  value       = "${var.prefix}"
  description = "Prefix configured for lifecycle rules"
}

output "enabled" {
  value       = "${var.enabled}"
  description = "Is module enabled"
}

output "bucket_name" {
  description = "Bucket Name (aka ID)"
  value = "${var.bucket_name}"
}
#########################
# AWS Provider us-east-1
#########################
provider "aws" {
  region                                = "${var.region}"
  shared_credentials_file               = "${pathexpand("~/.aws/credentials")}"
  profile                               = "${var.project_namespace}"
}

data "aws_caller_identity" "current" {}

##################
# Local Veriables
##################
locals {
  s3_bucket_prefix                      = "${var.project_namespace}-${var.env}"

  tags = {
      cluster                           = "${local.eks_cluster_name}"
      vpc                               = "${local.vpc_name}"
      environment                       = "${var.env}"
      owners                            = "snappay"
  }
}

###########################################
# snappay-nonprod-snappay bucket
###########################################
data "aws_iam_policy_document" "snappay-nonprod-snappay" {
  statement {
    sid = "s3bucketPolicy"
    effect = "Allow"

    actions = [
      "s3:*",]

    principals {
      identifiers = [
        "eks.amazonaws.com",
        "ec2.amazonaws.com"]
      type = "Service"
    }
    resources = ["arn:aws:s3:::${module.snappay-nonprod-snappay.bucket_name}/*"]
  }

  statement {
    sid     = "s3bucketSourceIPs"
    effect  = "Allow"

    actions = ["s3:*",]
    condition {
      test = "IpAddress"
      variable = "aws:SourceIp"
      values = []
    }

    principals {
      identifiers = ["*"]
      type = "*"
    }

    resources = ["arn:aws:s3:::${module.snappay-nonprod-snappay.bucket_name}/*"]
  }
}

module "snappay-nonprod-snappay" {
  source             = "../../../../../modules/s3"
  bucket_name        = "${local.s3_bucket_prefix}-snappay.ca"
  enabled            = "true"
  versioning_enabled = "true"
  force_destroy      = "true"
  policy             = "${data.aws_iam_policy_document.snappay-nonprod-snappay.json}"
  tags               = "${merge(var.tags)}"
}

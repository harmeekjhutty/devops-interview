############
### VPC
############
project_namespace                   = "snappay-ca"
env                                 = "nonprod"
region                              = "us-east-1"
cidr                                = "10.0.0.0/21"
customer_gateway_ip_address         = "x.x.x.x"
azs                                 = ["us-east-1a", "us-east-1b"]
public_subnets                      = ["10.0.0.0/24", "10.0.1.0/24"]
private_subnets                     = ["10.0.2.0/24", "10.0.3.0/24"]
create_vpn_connection               = false
vpn_connection_static_routes_only   = false
enable_dhcp_options                 = false
create_customer_gateway             = false
dhcp_options_domain_name            = "snappay.ca"
tags                                = { "orchestration" = "terraform", "owner" = "devops", "env" = "nonprod" }
